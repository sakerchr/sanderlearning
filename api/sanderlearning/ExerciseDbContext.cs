﻿using Microsoft.EntityFrameworkCore;
using sanderlearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace sanderlearning
{
    public class ExerciseDbContext : DbContext
    {
        public ExerciseDbContext(DbContextOptions<ExerciseDbContext> dbContextOptions) : base(dbContextOptions)
        {
        }
        public DbSet<Exercise> Exercises { get; set; }

    }
}
